object Bigtablecycledata extends App {

  val tableName = args(0)
  val df1 = spark.read.format("csv").option("header", "true").load("/data/cycle_ride.csv")

  import org.apache.spark.sql.SparkSession

  val spark = SparkSession.builder.appName(appName).getOrCreate

  import spark.implicits._

  import org.apache.spark.sql.execution.datasources.hbase.HBaseTableCatalog

  val format = "org.apache.spark.sql.execution.datasources.hbase"
  val opts = Map(
    HBaseTableCatalog.tableCatalog -> df1,
    HBaseTableCatalog.newTable -> "5")
    .write
    .options(opts)
    .format(format)
    .save

  val df = spark
    .read
    .option(HBaseTableCatalog.tableCatalog, df1)
    .format(format)
    .load
  
  df.cache
  df.show


  df.createOrReplaceTempView("table1")
  spark
    .sql("select * from table1")
    .show

  spark.stop
}

case class BigtableRecord(
  cust_id: String,
  record_id: Int,
  commute: String,
  distance: Int,
  elapsed_time:Int,
  kilojoules:Int,
  moving_time:Int,
  name:String,
  start_date_local:String,
  start_date_local_tms:String,
  )

object BigtableRecord {
  def apply(i: Int): BigtableRecord = {
    val s = s"""row${"%03d".format(i)}"""
    BigtableRecord(s,
      i % 2 == 0,
      i.toDouble,
      i)
  }
}