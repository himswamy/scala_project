package com.example.bigtable.spark.shc

import com.google.cloud.hadoop.io.bigquery.BigQueryConfiguration
import com.google.cloud.hadoop.io.bigquery.BigQueryFileFormat
import com.google.cloud.hadoop.io.bigquery.GsonBigQueryInputFormat
import com.google.cloud.hadoop.io.bigquery.output.BigQueryOutputConfiguration
import com.google.cloud.hadoop.io.bigquery.output.IndirectBigQueryOutputFormat
import com.google.gson.JsonObject
import org.apache.hadoop.io.LongWritable
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.execution.datasources.hbase.HBaseTableCatalog


object cycledata extends App {

    @transient
    val conf = sc.hadoopConfiguration

    val df1 = spark.read.format("csv").option("header", "true").load("/data/cycle_ride.csv")

    val fullyQualifiedInputTableId = "cycle_ride.csv"
    val projectId = conf.get("fs.gs.project.id")
    val bucket = conf.get("fs.gs.system.bucket")
    conf.set(BigQueryConfiguration.PROJECT_ID_KEY, projectId)
    conf.set(BigQueryConfiguration.GCS_BUCKET_KEY, bucket)
    BigQueryConfiguration.configureBigQueryInput(conf, fullyQualifiedInputTableId)

    val outputTableId = "projectspark + :cycle_dataset.cycle_data"
    val outputGcsPath = ("gs://" + bucket + "/hadoop/cycle_data")

    BigQueryOutputConfiguration.configureWithAutoSchema(
        conf,
        outputTableId,
        outputGcsPath,
        BigQueryFileFormat.NEWLINE_DELIMITED_JSON,
        classOf[TextOutputFormat[_,_]])
      def convertToTuple(record: JsonObject) : (String, Long, Long, Long, Long, Long, Long, String, String, String) = {
        val cust_id = record.get("cust_id").getAsString.toLowerCase
        val record_id = record.get("record_id").getAsLong
        val commute = record.get("commute").getAsLong
        val distance = record.get("distance").getAsLong
        val elapsed_time = record.get("elapsed_time").getAsLong
        val kilojoules = record.get("kilojoules").getAsLong
        val moving_time = record.get("moving_time").getAsLong
        val name = record.get("name").getAsString.toLowerCase
        val start_date_local = record.get("start_date_local").getAsString.toLowerCase
        val start_date_local_tms = record.get("start_date_local_tms").getAsString.toLowerCase
        return (cust_id, record_id, commute, distance, elapsed_time, kilojoules, moving_time, name, start_date_local, start_date_local_tms)
      }
      def convertToJson(pair: (String, Long, Long, Long, Long, Long, Long, String, String, String)) : JsonObject = {
        val cust_id = pair._1
        val record_id = pair._2
        val commute = pair._3
        val distance = pair._4
        val elapsed_time = pair._5
        val kilojoules = pair._6
        val moving_time = pair._7
        val name = pair._8
        val start_date_local = pair._9
        val start_date_local_tms = pair._10
        val jsonObject = new JsonObject()
        jsonObject.addProperty("cust_id", cust_id)
        jsonObject.addProperty("record_id", record_id)
        jsonObject.addProperty("commute", commute)
        jsonObject.addProperty("distance", distance)
        jsonObject.addProperty("elapsed_time", elapsed_time)
        jsonObject.addProperty("kilojoules", kilojoules)
        jsonObject.addProperty("moving_time", moving_time)
        jsonObject.addProperty("name", name)
        jsonObject.addProperty("start_date_local", start_date_local)
        jsonObject.addProperty("start_date_local_tms", start_date_local_tms)
        return jsonObject
      }

      df1.show(5)

      (df1
         .map(pair => (null, convertToJson(pair)))
         .saveAsNewAPIHadoopFile(
         hadoopConf.get(BigQueryConfiguration.TEMP_GCS_PATH_KEY), 
         classOf[String], 
         classOf[JsonObject], 
         classOf[BigQueryOutputFormat[String, JsonObject]], hadoopConf))

  }